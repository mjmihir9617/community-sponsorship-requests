# Community Sponsorship Requests

This project is meant to help us receive and triage community sponsorship requests as per the [Community Relations Handbook page about community sponsorship requests](https://about.gitlab.com/handbook/marketing/community-relations/index.html#community-sponsorship-requests). 

For questions, please contact community@gitlab.com
